Dedicated Core FILES ONLY

Current version:  1.8

You will need to copy the Data/Config, Data/Prefabs, and Mods folders from the Client version branch
located at:  https://gitlab.com/Medieval/EmuBranch


Dedicated server installation instructions:

Copy the folders from either the Gitlab directly or Mod Launcher downloaded folder into your server directory

  7DaysToDieServer_Data
  Data/Bundles/modterraintexturesD
  Data/Config
  Data/Prefabs
  Mods
  
  
** IMPORTANT **

Do Not copy the following files from the client to the Dedicated folders or it will cause errors:


Do NOT copy any resource asset file in the client folder 7DaysToDie_Data file including these:

 medievalsounds.resource
 
 menumusic.resource
 
 resources.assets
 
 resources.resource
 
 sharedassets0.assets
 
 sharedassets1.assets

 
 
Do NOT copy these files:

 Data/Bundle/BlockTextureAtlases
 
 Data/Bundle/TerrainTextures
 


Refer to EmuBranch for changes.

A special version of Medieval Mod that changes several elements:

Increased chance for ultra rare prefabs to spawn in a random world.

User interface changes to cater to displaying the most
types of languages and their respective symbols.

Less rain! Hooray!

Slight increase to certain incredibly rare loot items.

More stuff to come as time goes on.
